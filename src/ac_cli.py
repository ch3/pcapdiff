from sys import argv
#import analyse_cap
#from analyse_cap import parse
from analyse_cap import *

fn1 = argv[1]
fn2 = argv[2]

#p = PcapNgFile(fn1)
#ps = tuple ([p for p in p.packets])
#p = PcapNgFile(fn2)
#ps = tuple ([p for p in p.packets])

commands_0 = parse(fn1)
print ()
commands_1 = parse(fn2)

print ()
print_diff_subs (commands_0, commands_1)
print ()

print ('---------------------')
print ()

cmd_diff = diff (commands_0, commands_1)
print_diff (cmd_diff)
print ()

print ('---------------------')
print ()

#commands_0 = tuple (filter (lambda x: x.direction_str == '>', commands_0))
#commands_1 = tuple (filter (lambda x: x.direction_str == '>', commands_1))
commands_0 = get_sent_pkgs (commands_0)
commands_1 = get_sent_pkgs (commands_1)

cmd_diff = diff (commands_0, commands_1)
print_diff (cmd_diff)

#cmd_conf0 = filter (lambda x: x.cmd_id == 0x11, commands_0)
#cmd_conf1 = filter (lambda x: x.cmd_id == 0x11, commands_1)
#for elem0, elem1 in zip (cmd_conf0, cmd_conf1):
#    print ('command: {:0x}'.format (elem0.cmd_id))
#    #print (dir (elem0))
#    #print (dir (elem1))
#    elem0.diff (elem1)
#    #try:
#    #    elem0.diff (elem1)
#    #except:
#    #    print ('failed')
#    print ()


ids = []
for cmd in commands_0:
    ids.append (cmd.cmd_id)
cmds_0 = []
cmds_1 = []
for cmd_id in ids:
    cs_0 = tuple (filter (lambda x: x.cmd_id == cmd_id, commands_0))
    cs_1 = tuple (filter (lambda x: x.cmd_id == cmd_id, commands_1))
    l_min = min (len (cs_0), len (cs_1))
    cs_0 = cs_0[:l_min]
    cs_1 = cs_1[:l_min]
    cmds_0.extend (cs_0)
    cmds_1.extend (cs_1)

for elem0, elem1 in zip (cmds_0, cmds_1):
    print ('command: {:0x}'.format (elem0.cmd_id))
    #print (dir (elem0))
    #print (dir (elem1))
    elem0.diff (elem1)
    #try:
    #    elem0.diff (elem1)
    #except:
    #    print ('failed')
    print ()


