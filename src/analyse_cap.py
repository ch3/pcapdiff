from pypcappy import PcapNgFile
from struct import unpack

class Command:
    def __init__(self, pkgs:list):
        self.cmd_pkg = pkgs[0]
        self.sts_pkg = pkgs[-1]
        self.payload_pkgs = pkgs[1:-1]
        # This is a quick fix/hack so that the diff is able to properly compare
        # (between windows and linux)
        self.payload_pkgs_strip = tuple (filter (lambda x: len (x) != 0, self.payload_pkgs))

        assert (self.cmd_pkg[0:3] == b'CMD')
        assert (self.sts_pkg[0:3] == b'STS')

        self.cmd_id = unpack (">L", self.cmd_pkg[4:8])[0]
        assert (self.cmd_id == unpack (">L", self.sts_pkg[4:8])[0])

        self.direction = unpack ("B", self.cmd_pkg[12:13])[0]
        assert (self.direction == 0 or self.direction == 0x80)
        self.direction_str = '>' if self.direction == 0 else '<'
        self.len = unpack (">L", self.cmd_pkg[8:12])[0]

        self.num_pkgs = len (pkgs) - 2
        #print (self.num_pkgs)

        self.cmd_unknown1 = unpack ("B", self.cmd_pkg[13:14])[0]
        self.cmd_unknown2 = unpack (">H", self.cmd_pkg[14:16])[0]
        #print (self.cmd_unknown1)
        #print (self.cmd_unknown2)

        self.cmd_reserved = unpack (">L", self.cmd_pkg[16:20])[0]
        assert (self.cmd_reserved == 0)

        self.sts_sts = unpack (">L", self.sts_pkg[8:12])[0]
        self.sts_sts_str = 'success' if self.sts_sts == 0 else 'failed'

    def __str__(self):
        return 'Command {:02X} {} {} bytes ({})'.format (self.cmd_id, self.direction_str, self.len, self.sts_sts_str)

    def __eq__(self, other):
        return self.cmd_id == other.cmd_id

    def print_contents (self):
        for attr, value in self.__dict__.items():
            if isinstance (value, tuple) and isinstance (value[0], bytes):
                print ('bytes')
            elif isinstance (value, bytes):
                print ('bytes')
            else:
                print ('{}: {}'.format (attr, value))

    def diff (self, other):
        assert (self.cmd_id == other.cmd_id)
        other_dict = other.__dict__
        for attr, value in self.__dict__.items():
            #print ('\t{}: {}'.format (attr, type (value)))
            if isinstance (value, tuple) and isinstance (value[0], bytes):
                print ('comparing typle of bytes ({})'.format (attr))
                for j, pkg0, pkg1 in zip (range (len (value)), value, other_dict[attr]):
                    differences = []
                    for i, c0, c1 in zip (range (len (pkg0)), pkg0, pkg1):
                        if c0 != c1:
                            differences.append ((i, c0, c1))
                    if 0 != len (differences):
                        print ('difference in {} ({}):'.format (attr, j))
                        for (i, c0, c1) in differences:
                            print ('\t{: >3x}: 0x{:0>2x} | 0x{:0>2x}'.format (i, c0, c1))
            elif isinstance (value, bytes):
                print ('comparing bytes ({})'.format (attr))
                differences = []
                for i, c0, c1 in zip (range (len (value)), value, other_dict[attr]):
                    if c0 != c1:
                        differences.append ((i, c0, c1))
                if 0 != len (differences):
                    print ('difference in {}:'.format (attr))
                    for (i, c0, c1) in differences:
                        print ('\t{0: >3}: 0x{:0>2x} | 0x{:0>2x}"'.format (i, c0, c1))
            elif value != other_dict[attr]:
                print ('difference in {}:'.format (attr))
                print ('\t{}'.format (value))
                print ('\t{}'.format (other_dict[attr]))
                #print ('\ttype: {}'.format (type(value[0])))
            #print ('\t - {}:'.format (attr))
            #print ('\t\t - {}:'.format (value))
            #print ('\t\t - {}:'.format (other_dict[attr]))


class ModeConf (Command):
    def __init__(self, pkgs:list):
        super.__init__(self, pkgs)
        assert (self.cmd_id == 0x10)
        self._init_ConfCommand ()

    def _init_ConfCommand (self):
        pkg_conf = b''
        for pkg in self.payload_pkgs:
            if 36 == len (pkg) or 37 == len (pkg):
                pkg_conf = pkg
                break
        assert (b'' != pkg_conf)
        self.scanunit = unpack ("B", pkg_conf[0:1])[0]
        self.side =     unpack ("B", pkg_conf[1:2])[0]
        self.unknown0 = unpack (">H", pkg_conf[2:4])[0]

        self.unknown1 = unpack (">L", pkg_conf[4:8])[0]
        self.reserved0 = unpack (">L", pkg_conf[8:12])[0]

        self.reserved1 = unpack (">L", pkg_conf[12:16])[0]
        self.reserved2 = unpack (">L", pkg_conf[16:20])[0]

        self.reserved3 = unpack (">L", pkg_conf[20:24])[0]
        self.reserved4 = unpack (">L", pkg_conf[24:28])[0]

        self.unknown2 =  unpack (">L", pkg_conf[28:32])[0]
        self.unknown3 = unpack (">L", pkg_conf[32:36])[0]

class ImgConf (Command):
    def __init__(self, pkgs:list):
        super.__init__(self, pkgs)
        assert (self.cmd_id == 0x11)
        self._init_ConfCommand ()

    def _init_ConfCommand (self):
        pkg_conf = b''
        for pkg in self.payload_pkgs:
            if 88 == len (pkg) or 85 == len (pkg):
                pkg_conf = pkg
                break
        assert (b'' != pkg_conf)
        self.task = unpack ("B", pkg_conf[0:1])[0]
        self.depth = unpack ("B", pkg_conf[1:2])[0]
        self.scanunit = unpack ("B", pkg_conf[2:3])[0]
        self.mode = unpack ("B", pkg_conf[3:4])[0]

        self.resolution0 = unpack (">H", pkg_conf[4:6])[0]
        self.resolution1 = unpack (">H", pkg_conf[6:8])[0]

        # 0x08
        self.left = unpack (">L", pkg_conf[8:12])[0]
        self.top = unpack (">L", pkg_conf[12:16])[0]
        # 0x10
        self.width = unpack (">L", pkg_conf[16:20])[0]
        self.height = unpack (">L", pkg_conf[20:24])[0]

        # 0x18
        self.unknown0 = unpack (">L", pkg_conf[24:28])[0]
        self.unknown1 = unpack (">L", pkg_conf[28:32])[0]
        # 0x20
        self.unknown2    = unpack (">L", pkg_conf[32:36])[0]
        self.compression = unpack (">B", pkg_conf[36:37])[0]
        self.unknown30   = unpack (">B", pkg_conf[37:38])[0]
        self.unknown31   = unpack (">H", pkg_conf[38:40])[0]
        self.unknown4    = unpack (">L", pkg_conf[40:44])[0]
        self.unknown5    = unpack (">L", pkg_conf[44:48])[0]
        self.unknown6    = unpack (">L", pkg_conf[48:52])[0]
        self.unknown7    = unpack (">L", pkg_conf[52:56])[0]
        self.unknown8    = unpack (">L", pkg_conf[56:60])[0]

        self.unknown9  =     unpack ("B", pkg_conf[60:61])[0]
        self.unknown10 =     unpack ("B", pkg_conf[61:62])[0]
        self.rotate =        unpack ("B", pkg_conf[62:63])[0]
        self.cropping_mode = unpack ("B", pkg_conf[63:64])[0]

        self.unknown11 =   unpack (">H", pkg_conf[64:66])[0]
        self.unknown12 =   unpack ("B", pkg_conf[66:67])[0]
        self.blank_image = unpack ("B", pkg_conf[67:68])[0]

        self.blank_image_c = unpack ("B", pkg_conf[68:69])[0]
        self.unknown13 =     unpack ("B", pkg_conf[69:70])[0]
        self.unknown14 =     unpack (">H", pkg_conf[70:72])[0]

        self.unknown13 =    unpack ("B", pkg_conf[72:73])[0]
        self.unknown14 =    unpack ("B", pkg_conf[73:74])[0]
        self.unknown16 =    unpack ("B", pkg_conf[74:75])[0]
        self.colorsharpen = unpack ("B", pkg_conf[75:76])[0]

        self.unknown16 = unpack ("B", pkg_conf[76:77])[0]
        self.unknown17 = unpack ("B", pkg_conf[77:78])[0]
        self.channels  = unpack ("B", pkg_conf[78:79])[0]
        self.unknown18 = unpack ("B", pkg_conf[79:80])[0]

        self.unknown19 = unpack (">L", pkg_conf[80:84])[0]

        self.unknown20 = unpack ("B", pkg_conf[84:85])[0]


def determine_header_size (ps:tuple):
    # Another option would be to find the minimal sized package and assume it's
    # an empty one - the size of the header
    # This would also be more generic (for upcoming usecases with a different protocol)
    header_size_guess = 0
    for pkg in ps:
        if (100 > len (pkg)):
            i = 0
            while i < len (pkg) - 3:
                if pkg[i:i+3] == b'CMD':
                    #print ('found \'CMD\' at', i)
                    if 0 == header_size_guess:
                        header_size_guess = i
                        #print ('set guess to', i)
                    #else:
                    #    assert (header_size_guess == i)
                    break
                i += 1
            #print ("test")
    return header_size_guess

def find_cmd(idx:int, ps:tuple):
    while idx < len (ps):
        if ps[idx][0:3] == b'CMD':
            return idx
        idx += 1

def find_sts(idx:int, ps:tuple):
    if idx is None:
        return
    while idx < len (ps):
        if ps[idx][0:3] == b'STS':
            return idx
        idx += 1

def find_command (idx:int, ps:tuple):
    idx_cmd = find_cmd(idx, ps)
    idx_sts = find_sts(idx_cmd, ps)
    return (idx_cmd, idx_sts)


def parse (fn:str):
    commands = []
    p = PcapNgFile(fn)
    ps = tuple ([p.data for p in p.packets])
    header_size = determine_header_size (ps)
    lcd = tuple ([p[header_size:] for p in ps]) #leftover capture data
    idx = 0
    while idx < len (lcd):
        (idx_cmd, idx_sts) = find_command (idx, lcd)
        if idx_cmd is None:
            break
        cmd = Command(lcd[idx_cmd:idx_sts+1])
        if cmd.cmd_id == 0x11:
            cmd.__class__ = ImgConf
            cmd._init_ConfCommand()
        elif cmd.cmd_id == 0x10:
            cmd.__class__ = ModeConf
            cmd._init_ConfCommand()
            #print (cmd.resolution0)
        print (cmd)
        commands.append (cmd)
        #print ((idx_cmd, idx_sts, lcd[idx_cmd:idx_sts]))
        idx = idx_sts + 1
    #print (commands[0])
    return tuple(commands)

def find_next_match (seq:tuple, elem):
    for i, e in enumerate(seq):
        #print ("find_next_match: i: {}, e: {}, elem: {}".format(i, e, elem))
        if e == elem:
            #print ("find_next_match return:", i)
            return i
    return None

def find_common_substr_start (commands_0:tuple, commands_1:tuple):
    cnt = max (len (commands_0), len (commands_1)) + 1
    idx_0_match = None
    idx_1_match = None
    for i, elem in enumerate (commands_0):
        idx = find_next_match (commands_1, elem)
        if idx is not None and i + idx < cnt:
            idx_0_match = i
            idx_1_match = idx
            #print ("set idx_0_match: {}, idx_1_match: {} (cnt: {})".format (idx_0_match, idx_1_match, cnt))
            cnt = i + idx
    #print ("idx_0_match: {}, idx_1_match: {}".format (idx_0_match, idx_1_match))
    for i, elem in enumerate (commands_1):
        idx = find_next_match (commands_0, elem)
        if idx is not None and i + idx < cnt:
            idx_0_match = i
            idx_1_match = idx
            #print ("set idx_0_match: {}, idx_1_match: {} (cnt: {})".format (idx_0_match, idx_1_match, cnt))
            cnt = i + idx
    #print ("find_common_substr_start returning ({}, {})".format (idx_0_match, idx_1_match))
    return (idx_0_match, idx_1_match)

def find_common_substr_end (commands_0:tuple, commands_1:tuple):
    for i, elem in enumerate (commands_0):
        if elem != commands_1[i]:
            return i
    return len (commands_0)

def find_common_substr (commands_0:tuple, commands_1:tuple):
    #print ("find_common_substr called with")
    (idx_0, idx_1) = find_common_substr_start (commands_0, commands_1)
    substr_len = find_common_substr_end (commands_0[idx_0:], commands_1[idx_1:])
    #print ("find_common_substr returning ({}, {}, {})".format (idx_0, idx_1, substr_len))
    return (idx_0, idx_1, substr_len)

def print_diff_subs (subs0:tuple, subs1:tuple):
    for i in range(max (len (subs0), len (subs1))):
        str0 = " " * 42
        str1 = " " * 42
        if i < len (subs0):
            str0 = subs0[i].__str__()
        if i < len (subs1):
            str1 = subs1[i].__str__()
        str0 = '{0: >3}: {1: <42} | '.format(i, str0)
        print (str0 + str1)

def print_common_substr (common_substr:tuple):
    for i in range (len (common_substr)):
        print (' ' * 21 + common_substr[i].__str__())

def print_diff (cmd_diff:tuple):
    #print (len (cmd_diff))
    for i in cmd_diff:
        diff_substr = i[0]
        diff_substr_0 = diff_substr[0]
        diff_substr_1 = diff_substr[1]
        print_diff_subs (diff_substr_0, diff_substr_1)
        common_substr = i[1]
        #print ("common substr:", len (common_substr))
        print_common_substr (common_substr)

def diff (commands_0:tuple, commands_1:tuple):
    idx_0_last = 0
    idx_1_last = 0
    ret_seq = []
    while idx_0_last < len (commands_0) and idx_1_last < len (commands_1):
        #print ("idx0: {}, idx1: {}".format (idx_0_last, idx_1_last))
        (idx_0, idx_1, substr_len) =\
            find_common_substr (commands_0[idx_0_last:], commands_1[idx_1_last:])
        if idx_0 == None or idx_1 == None:
            # we're at the end
            print (idx_0_last, idx_0, substr_len)
            #idx_0_last = idx_0_last + idx_0 + substr_len
            print (idx_1_last, idx_1, substr_len)
            #idx_1_last = idx_1_last + idx_1 + substr_len
            break
        idx_0 = idx_0_last + idx_0
        idx_1 = idx_1_last + idx_1
        #print ("idx0: {}, idx1: {}".format (idx_0, idx_1))
        #print ("found substr: {} {} ({})".format (idx_0, idx_1, substr_len))
        #print ("last: {} {}".format (idx_0_last, idx_1_last))
        # store
        diff_substr = (commands_0[idx_0_last:idx_0], commands_1[idx_1_last:idx_1])
        common_substr = (commands_0[idx_0:idx_0 + substr_len])
        ret_seq.append ((diff_substr, common_substr))
        idx_0_last = idx_0 + substr_len
        idx_1_last = idx_1 + substr_len
        #print ("idx0: {}, idx1: {}".format (idx_0, idx_1))
        #print_diff (ret_seq)
        #print ('diff:')
        #print_diff_subs (diff_substr[0], diff_substr[1])
        #print ('common:')
        #print_common_substr (common_substr)
        #print ()
    #print ("idx0: {}, idx1: {}".format (idx_0, idx_1))
    #print ("last idx0: {}, idx1: {}".format (idx_0_last, idx_1_last))
    #print ("len0: {}, len1: {}".format (len (commands_0), len (commands_1)))
    diff_substr = (commands_0[idx_0_last:], commands_1[idx_1_last:])
    ret_seq.append ((diff_substr, ()))
    return ret_seq

def get_sent_pkgs (commands:tuple):
    return tuple (filter (lambda x: x.direction_str == '>', commands))

def filter_pkgs (cmd_type:int):
    cmds_conf_l = list(filter (lambda x: x.cmd_id == cmd_type, cs))
