# pcapdiff

In the current state these are two scripts I used for a project to diff two
captures of network traffic.

(More precisely, I was using it to diff the traffic on a USB cable from a
proprietary driver and my own attemt to create similar functionality.)

As I scanned on different systems (Windows and GNU/Linux), some header(s) seem
to have different sizes - the scripts respect that.

